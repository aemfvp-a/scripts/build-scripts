#!/usr/bin/env bash

# Copyright (c) 2024, ARM Limited and Contributors. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# Neither the name of ARM nor the names of its contributors may be used
# to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

#
# This script uses the following environment variables from the variant
#
# VARIANT - build variant name
# TOP_DIR - workspace root directory
# CROSS_COMPILE - PATH to GCC including CROSS-COMPILE prefix
# TF_A_ARCH - aarch64 or aarch32
# TF_A_BUILD_ENABLED - Flag to enable building ARM Trusted Firmware
# TF_A_RME_BUILD_ENABLED - Flag to enable building ARM Trusted Firmware with
# 	support for RME
# TF_A_PATH - sub-directory containing ARM Trusted Firmware code
# TF_A_PLATS - List of platforms to be built (from available in tf-a/plat)
# TF_A_DEBUG_ENABLED - 1 = debug, 0 = release build
# TF_A_BUILD_TYPE - The type of build to perform
# TF_A_BUILD_FLAGS - Additional build flags to pass on the build command line
# TF_A_TBBR_BUILD_FLAGS - command line options to enable TBBR in ARM TF build
# OPTEE_BUILD_ENABLED - Flag to indicate if optee is enabled
# TBBR_{plat} - array of platform parameters, indexed by
# 	tbbr - flag to indicate if TBBR is enabled
# OUTPUT_DIR - Directory where build products are stored
# TF_A_GCC_PATH - Path where the compiler is installed
# VARIANT_ELF - The compiler variant

# newer_ctime COMPARE_FILE CHECK_FILE...
#
# Returns TRUE if any of...
# 1. the input do not exist the return value is TRUE.
# 2. CHECK_FILE... has a newer ctime than COMPARE_FILE.
newer_ctime() {
	local f
	for f in "$@" ; do
		[[ -e "$f" ]] || return 0
	done

	local -r state_file="$1" ; shift
	[[ -n $(find "$@" -cnewer "$state_file" -print -quit) ]]
}

do_build ()
{
	echo
	echo -e "${GREEN}Building TF-A for $PLATFORM on [`date`]${NORMAL}"
	echo

	if [ "$TF_A_BUILD_ENABLED" == "1" ]; then
		local -r OPENSSL_PATH="$PLATDIR/components/host_openssl"
		local -r OPENSSL_SRC="$TOP_DIR/tools/openssl"
		local -r state_file="$OPENSSL_PATH/host.openssl.state"

		if newer_ctime "$state_file" \
			"$OPENSSL_SRC" \
			"$TOP_DIR/build-scripts/build-tf-a.sh" \
			"$TOP_DIR/build-scripts/framework.sh" \
		; then
			rm -rf "$OPENSSL_PATH"
			mkdir -p "$OPENSSL_PATH/build"
			cd "$OPENSSL_PATH/build"
			CROSS_COMPILE= $OPENSSL_SRC/Configure \
				--libdir=lib \
				--prefix="$OPENSSL_PATH/install" \
				--api=1.0.1 \
				no-shared \
				no-dso \
				no-threads
			make -j$PARALLELISM
			make -j$PARALLELISM install_sw
			cd -
			touch "$state_file"
			echo "OpenSSL Installed"
		fi

		pushd $TOP_DIR/$TF_A_PATH
		for plat in $TF_A_PLATS; do
			local atf_build_flags=$TF_A_BUILD_FLAGS
			local atf_tbbr_enabled=TARGET_$TARGET_BINS_PLATS[tbbr]
			local atf_optee_enabled=OPTEE_BUILD_ENABLED
			if [ "${!atf_tbbr_enabled}" == "1" ]; then
				#if trusted board boot(TBBR) enabled, set corresponding compiliation flags
				atf_build_flags="${atf_build_flags} $TF_A_TBBR_BUILD_FLAGS"
			fi
			if [ "${!atf_optee_enabled}" == "1" ]; then
				#if optee enabled, set corresponding compiliation flags
				atf_build_flags="${atf_build_flags} ARM_TSP_RAM_LOCATION=$OPTEE_RAM_LOCATION"
				atf_build_flags="${atf_build_flags} SPD=opteed"
			fi
			if [ "$TF_A_AARCH32_EL3_RUNTIME" == "1" ]; then
				CROSS_COMPILE=$CROSS_COMPILE_32 \
				make -j $PARALLELISM PLAT=$plat ARCH=aarch32 DEBUG=$TF_A_DEBUG_ENABLED $TF_A_BL32_FLAGS bl32
				rm -rf build/juno/release/lib*
				targets="bl1 bl2 bl31"
			else
				targets="all"
			fi
			# HACK: this is to deal with juno32 building TF-A BL1 and BL2 as Aarch64
			#       but everything else as Aarch32
			if [ "$TF_A_ARCH" == "aarch32" ]; then
				TMP_CROSS_COMPILE=$CROSS_COMPILE_32
			else
				TMP_CROSS_COMPILE=$CROSS_COMPILE_64
			fi
			CROSS_COMPILE=$TMP_CROSS_COMPILE \
			make -j $PARALLELISM PLAT=$plat ARCH=$TF_A_ARCH DEBUG=$TF_A_DEBUG_ENABLED ${atf_build_flags} ${targets}
		done

		# make tools
		make OPENSSL_DIR="$OPENSSL_PATH/install" -j $PARALLELISM certtool fiptool
		popd
	fi

	if [ "$TF_A_RME_BUILD_ENABLED" == "1" ]; then
		pushd $TF_A_PATH
		# Set Compiler
		export CROSS_COMPILE=$VARIANT_ELF-
		export PATH=$TF_A_GCC_PATH:$PATH

		# Set debug flag, based on user choice
		local tfa_debug_flag
		[ "${TF_A_BUILD_TYPE}" == "debug" ] && tfa_debug_flag=1 || tfa_debug_flag=0

		# Build TF-A for Standard Tests
		if [ "${TFTF_STANDARD_TESTS}" == "1" ]; then
			make realclean
			# Build flags needed are 4-World only
			BUILD_FLAGS="${TF_A_4W_FLAGS}"
			make -j$PARALLELISM \
				PLAT=$TF_A_PLATS \
				ARCH=$TF_A_ARCH \
				DEBUG=$tfa_debug_flag \
				${BUILD_FLAGS} \
				BL33=$OUTPUT_PLATFORM_DIR/tftf-std-tests.bin \
				all fip
			cp ${TF_A_PATH}/build/${TF_A_PLATS}/${TF_A_BUILD_TYPE}/fip.bin $OUTPUT_PLATFORM_DIR/fip-std-tests.bin
			# Remove the tftf.bin as not needed after packed in fip
			rm -f $OUTPUT_PLATFORM_DIR/tftf-std-tests.bin
		fi

		# Build TF-A for UEFI distro boot on FVP.
		if [ "${UEFI_RME_BUILD_ENABLED}" == "1" ]; then
			make realclean
			# Build flags needed for fvp UEFI boot.
			BUILD_FLAGS="${TF_A_4W_UEFI_FLAGS}"
			make -j$PARALLELISM \
				PLAT=$TF_A_PLATS \
				ARCH=$TF_A_ARCH \
				DEBUG=$tfa_debug_flag \
				${BUILD_FLAGS} \
				BL33=$TF_A_FVP_EFI \
				all fip
			# Copy bl1 and fip for uefi fvp boot.
			cp ${TF_A_PATH}/build/${TF_A_PLATS}/${TF_A_BUILD_TYPE}/fip.bin $OUTPUT_PLATFORM_DIR/fip-uefi.bin
			cp ${TF_A_PATH}/build/${TF_A_PLATS}/${TF_A_BUILD_TYPE}/bl1.bin $OUTPUT_PLATFORM_DIR/bl1-uefi.bin

		fi

		# Build TF-A for normal boot flow (no tests on fip binary).
		make realclean
		BUILD_FLAGS="${TF_A_4W_FLAGS} ${TF_A_PRELOADED_KERNEL}"
		make -j$PARALLELISM \
			PLAT=$TF_A_PLATS \
			ARCH=$TF_A_ARCH \
			DEBUG=$tfa_debug_flag \
			${BUILD_FLAGS} \
			all fip
		popd
	fi
}

do_clean ()
{
	echo
    echo -e "${GREEN}Cleaning TF-A for $PLATFORM on [`date`]${NORMAL}"
    echo

	if [ "$TF_A_BUILD_ENABLED" == "1" ]; then
		pushd $TOP_DIR/$TF_A_PATH

		for plat in $TF_A_PLATS; do
			make PLAT=$plat DEBUG=$TF_A_DEBUG_ENABLED clean
		done
		if [ -e tools/fip_create/Makefile ]; then
			make -C tools/fip_create clean
		fi
		if [ -e tools/fiptool/Makefile ]; then
			make -C tools/fiptool clean
		fi
		popd

		rm -rf "$PLATDIR/components/host_openssl"
	fi

	if [ "$TF_A_RME_BUILD_ENABLED" == "1" ]; then
		pushd $TF_A_PATH
		make realclean
		popd

		# Remove the bins copied to output-products
		rm -rf $OUTPUT_PLATFORM_DIR/bl1.bin
		rm -rf $OUTPUT_PLATFORM_DIR/fip.bin
		rm -rf $OUTPUT_PLATFORM_DIR/fip-std-tests.bin
	fi
}

do_package ()
{
	echo
    echo -e "${GREEN}Packaging TF-A for $PLATFORM on [`date`]${NORMAL}"
    echo

	if [ "$TF_A_BUILD_ENABLED" == "1" ]; then
		echo "Packaging tf-a... $VARIANT";
		# Copy binaries to output folder
		pushd $TOP_DIR
		for plat in $TF_A_PLATS; do
			mkdir -p ${OUTDIR}/$plat
			local mode=release
			[ "$TF_A_DEBUG_ENABLED" == "1" ] && mode=debug
			for bin in $TOP_DIR/$TF_A_PATH/build/$plat/${mode}/bl*.bin; do
				cp ${bin} ${OUTDIR}/$plat/tf-$(basename ${bin})
			done
			if [ -e $TOP_DIR/$TF_A_PATH/build/$plat/${mode}/fdts ]; then
				cp $TOP_DIR/$TF_A_PATH/build/$plat/${mode}/fdts/*.dtb ${OUTDIR}/$plat/
			fi
		done
		popd
	fi

	if [ "$TF_A_RME_BUILD_ENABLED" == "1" ]; then
		cp ${TF_A_PATH}/build/${TF_A_PLATS}/${TF_A_BUILD_TYPE}/bl1.bin $OUTPUT_PLATFORM_DIR/bl1.bin
		cp ${TF_A_PATH}/build/${TF_A_PLATS}/${TF_A_BUILD_TYPE}/fip.bin $OUTPUT_PLATFORM_DIR/fip.bin
	fi
}

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $DIR/framework.sh $@
