#!/usr/bin/env bash

# Copyright (c) 2023, ARM Limited and Contributors. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# Neither the name of ARM nor the names of its contributors may be used
# to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# This script uses the following environment variables from the variant
#
# KVM_UNIT_TESTS_PATH - Directory containing Kvm-Unit-Tests source
# KVM_UNIT_TESTS_GCC_PATH - Path where the compiler is installed
# KVM_UNIT_TESTS_ARCH - Architecture type used to build
# VARIANT_LINUX_GNU - The compiler variant
#

do_build ()
{
	echo
	echo -e "${GREEN}Building Kvm-Unit-Tests for $PLATFORM on [`date`]${NORMAL}"
	echo

	# Set Compiler
	export CROSS_COMPILE=$KVM_UNIT_CROSS_COMPILE
	export PATH=$KVM_UNIT_TESTS_GCC_PATH:$PATH

	pushd $KVM_UNIT_TESTS_PATH
	./configure --arch=$KVM_UNIT_TESTS_ARCH --cross-prefix=$CROSS_COMPILE --target=kvmtool
	make -j$PARALLELISM
	# Replace HOST=x86_64 to HOST=aarch64 due to cross-compiling
	sed -i -e 's/HOST=x86_64/HOST=aarch64/' config.mak
	popd
}

do_clean ()
{
	echo
	echo -e "${GREEN}Cleaning Kvm-Unit-Tests for $PLATFORM on [`date`]${NORMAL}"
	echo

	pushd $KVM_UNIT_TESTS_PATH
	./configure --arch=$KVM_UNIT_TESTS_ARCH --cross-prefix=$CROSS_COMPILE --target=kvmtool
	make distclean
	popd
}

do_package ()
{
	echo
	echo -e "${GREEN}Packing Kvm-Unit-Tests for $PLATFORM on [`date`]${NORMAL}"
	echo

	# Dummy call, Kvm-unit-tests are overlayed in buildroot.
}


DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $DIR/framework.sh $@