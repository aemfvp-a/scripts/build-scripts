#!/bin/bash

# Copyright (c) 2024, Arm Limited and Contributors. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# Neither the name of ARM nor the names of its contributors may be used
# to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

echo "launching ssh connection test from host vm"

echo "Setting up bridge device for guest network"
/realm/utils/kvm-bridge create
ifconfig vbr0

echo "Launching lkvm realm guest"
lkvm run --name rlvm1 --realm --restricted_mem -c 2 -m 256 --irqchip gicv3  --console virtio --network mode=tap,script=/realm/utils/kvm-ifup-vbr0,guest_mac=02:15:15:15:15:15 -k "/realm/Image" -d "/realm/realm-fs.ext4" 2>&1 | tee rlvm1.log &

# Get PID of the background process
pid=$!

# Wait for log file to be created - above is running async in background.
until [ -f rlvm1.log ]; do sleep 0.1; done

# Wait for Linux to boot. This banner proves EL0 is up and running.
grep -q "Welcome to Buildroot" <(tail -f rlvm1.log)

echo "lkvm guest boot complete"
ping -c 3 192.168.33.190

ssh -y root@192.168.33.190 pwd
rc1=$?
if [ $rc1 -eq 0 ]; then
    echo -e "SSH from Host to Guest Successful\n"
else
    echo -e "SSH from Host to Guest Failed\n"
fi

echo "Stop lkvm guest"
lkvm stop --name rlvm1

# Destroy bridge device.
echo "Removing the bridge device for guest network."
/realm/utils/kvm-bridge destroy
