#!/usr/bin/env bash

# Copyright (c) 2024, ARM Limited and Contributors. All rights reserved.
# Copyright (c) 2024, Linaro Limited. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# Neither the name of ARM nor the names of its contributors may be used
# to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# This script uses the following environment variables from the variant
#
# BUILDROOT_PATH - Directory containing the buildroot source
# LINUX_PATH - Directory containing the linux source
# BUILDROOT_GCC_PATH - Path where the compiler is installed
# VARIANT_LINUX_GNU - The compiler variant
# BUILDROOT_CONFIG_FILE - Path of the configuration file used to build
# OUTPUT_DIR - Directory where build products are stored
#

copy_dir_to_image() {
        for entry in $1/*;do
                source=$entry
                target=$(echo $entry | sed 's/rootfs//')
                echo "source=$source"
                echo "target=$target"
                echo "e2cp $source $2:$target"
                e2cp $source $2:$target
        done
}

do_build ()
{
	echo
	echo -e "${GREEN}Building Buildroot for $PLATFORM on [`date`]${NORMAL}"
	echo

	pushd $BUILDROOT_PATH

	# Build realm-fs
	ARCH=$(uname -m)
	cp ${BUILDROOT_CONFIG_FILE} .config
	./utils/config --set-val BR2_ROOTFS_OVERLAY "\"${ROOTFS_OVERLAY}\""
	if [ "$ARCH" == "x86_64" ]; then
		./utils/config --set-val BR2_TOOLCHAIN_EXTERNAL_PATH "\"/opt/toolchains/arm-gnu-toolchain-${GCC_VERSION}-${HOST_ARCH}-aarch64-none-linux-gnu/\""
		./utils/config --set-val BR2_TOOLCHAIN_EXTERNAL_PREFIX "\"aarch64-none-linux-gnu\""
		./utils/config --set-val BR2_TOOLCHAIN_EXTERNAL_CUSTOM_PREFIX "\"aarch64-none-linux-gnu\""
	        echo "INFO: building with $(uname -m)"
	elif [ "$ARCH" == "aarch64" ]; then
		./utils/config --enable BR2_TOOLCHAIN_BUILDROOT
		./utils/config --disable BR2_TOOLCHAIN_EXTERNAL
		./utils/config --set-val BR2_TOOLCHAIN_BUILDROOT_VENDOR "\"buildroot\""
	fi

	make olddefconfig
	make BR2_JLEVEL=$PARALLELISM

	# Build host-fs
	# Prepare overlay for realm (filesystem+kernel)
	mkdir -p $PWD/tmp_realm_overlay/realm
	cp $BUILDROOT_PATH/output/images/rootfs.ext4 ./tmp_realm_overlay/realm/realm-fs.ext4
	e2fsck -fp ./tmp_realm_overlay/realm/realm-fs.ext4
	resize2fs ./tmp_realm_overlay/realm/realm-fs.ext4 200M
	cp $LINUX_PATH/arch/arm64/boot/Image ./tmp_realm_overlay/realm/.

	# Copy networking utils.
	mkdir -p $PWD/tmp_realm_overlay/realm/utils
	cp ${GUEST_NETWORK_UTILS}/* $PWD/tmp_realm_overlay/realm/utils/

	# Prepare overlay for kvm-unit-tests
	mkdir -p $PWD/tmp_kvm_overlay/
	cp -R $KVM_UNIT_TESTS_PATH ./tmp_kvm_overlay/.

	# Prepare overlay for uefi realm distro boot, only if satadisk is created.
	mkdir -p $PWD/tmp_uefi_overlay/realm_uefi
	if [ "$UEFI_RME_BUILD_ENABLED" == "1" ]; then
		# Increase buildroot filesystem size to accomodate ubuntu image.
		./utils/config --set-str BR2_TARGET_ROOTFS_EXT2_SIZE "7200M"

		export INITRD_VERSION="6.1.0-26"
		export NEW_INITRD_VERSION=$(cat ${TOP_DIR}/linux/include/generated/utsrelease.h | cut -d "\"" -f 2)
		export NEW_INITRD_VERSION=${NEW_INITRD_VERSION::-6}

		if [ ! -f ${TOP_DIR}/downloads/${DISTRO} ]; then
			echo "downloads does not exist"
			mkdir -p ${TOP_DIR}/downloads
			wget ${DISTRO_LINK} -O ${TOP_DIR}/downloads/${DISTRO}
		fi
		rm -rf ${TOP_DIR}/distro-images
		mkdir -p ${TOP_DIR}/distro-images
		cp -r ${TOP_DIR}/downloads/${DISTRO} ${TOP_DIR}/distro-images
		mkdir -p distro
		dd if=${TOP_DIR}/distro-images/${DISTRO} bs=512 skip=262144 count=3930112 of=${TOP_DIR}/distro-images/${DISTRO}1 conv=notrunc

		mkdir -p ${TOP_DIR}/rootfs
		dpkg-deb -xv ${TOP_DIR}/linux/out/aemfvp-a-rme/debs/linux-image-${NEW_INITRD_VERSION}-dirty_${NEW_INITRD_VERSION}-2_arm64.deb ${TOP_DIR}/rootfs
		cd ${TOP_DIR}
		copy_dir_to_image rootfs/usr/share/doc/linux-image-${NEW_INITRD_VERSION}-dirty/ distro-images/${DISTRO}1
		copy_dir_to_image rootfs/usr/lib/linux-image-${NEW_INITRD_VERSION}-dirty/ distro-images/${DISTRO}1
		copy_dir_to_image rootfs/lib/modules/${NEW_INITRD_VERSION}-dirty/ distro-images/${DISTRO}1
		copy_dir_to_image rootfs/boot/ distro-images/${DISTRO}1
		cd -

		#rm -rf ${TOP_DIR}/initrd
		mkdir -p ${TOP_DIR}/initrd
		e2cp ${TOP_DIR}/distro-images/${DISTRO}1:/boot/initrd.img-${INITRD_VERSION}-arm64 ${TOP_DIR}/initrd/
		mv ${TOP_DIR}/initrd/initrd.img-${INITRD_VERSION}-arm64 ${TOP_DIR}/initrd/initrd.img-${INITRD_VERSION}-arm64.zst
		zstd -d ${TOP_DIR}/initrd/initrd.img-${INITRD_VERSION}-arm64.zst
		mkdir -p ${TOP_DIR}/initrd/initrd
		cd ${TOP_DIR}/initrd/initrd
		cpio -id < ../initrd.img-${INITRD_VERSION}-arm64
		#rm -rf lib/modules/${INITRD_VERSION}-arm64/
		cp -r ${TOP_DIR}/rootfs/lib/modules/${NEW_INITRD_VERSION}-dirty/ lib/modules/
		find . | cpio --create --format='newc' > ${TOP_DIR}/initrd/initrd.img-${NEW_INITRD_VERSION}-dirty-2-arm64
		cd -
		zstd ${TOP_DIR}/initrd/initrd.img-${NEW_INITRD_VERSION}-dirty-2-arm64
		e2cp ${TOP_DIR}/initrd/initrd.img-${NEW_INITRD_VERSION}-dirty-2-arm64.zst ../distro-images/debian-12-nocloud-arm64.raw1:/boot/initrd.img-${NEW_INITRD_VERSION}-dirty-2-arm64

		j2 ${TOP_DIR}/build-scripts/configs/aemfvp-a-rme/grub/grub.cfg.j2 > ${TOP_DIR}/build-scripts/configs/aemfvp-a-rme/grub/grub.cfg
		e2cp ${TOP_DIR}/build-scripts/configs/aemfvp-a-rme/grub/grub.cfg ${TOP_DIR}/distro-images/debian-12-nocloud-arm64.raw1:/boot/grub/

		dd if=${TOP_DIR}/distro-images/${DISTRO}1 bs=512 seek=262144 count=3930112 of=${TOP_DIR}/distro-images/${DISTRO} conv=notrunc

		cp ${TOP_DIR}/Build/ArmVirtKvmTool-AARCH64/DEBUG_GCC5/FV/KVMTOOL_EFI.fd $PWD/tmp_uefi_overlay/realm_uefi/
		cp ${DISTRO_IMAGE_PATH} $PWD/tmp_uefi_overlay/realm_uefi/
	fi

	# Enable build of KvmTool & set up the path
	sed -i 's/#\sBR2_PACKAGE_KVMTOOL\sis\snot\sset/BR2_PACKAGE_KVMTOOL=y/' .config
	echo "KVMTOOL_OVERRIDE_SRCDIR = ${KVM_TOOL_PATH}" > local.mk
	# Set the overlays needed on the host-fs
	./utils/config --set-val BR2_ROOTFS_OVERLAY "\"${ROOTFS_OVERLAY} $PWD/tmp_realm_overlay $PWD/tmp_kvm_overlay $PWD/tmp_uefi_overlay\""
	make oldconfig
	make -j$PARALLELISM kvmtool-rebuild
	make BR2_JLEVEL=$PARALLELISM

	# Remove the temporary overlays
	rm -rf $PWD/tmp_realm_overlay
	rm -rf $PWD/tmp_kvm_overlay
	rm -rf $PWD/tmp_uefi_overlay
	popd
}

do_clean ()
{
	echo
	echo -e "${GREEN}Cleaning Buildroot for $PLATFORM on [`date`]${NORMAL}"
	echo

	pushd $BUILDROOT_PATH
	make clean

	# Remove the temporary overlays if they are left in from previous incompelte builds
	rm -rf $PWD/tmp_realm_overlay
	rm -rf $PWD/tmp_kvm_overlay
	rm -rf $PWD/tmp_uefi_overlay
	rm -rf ${TOP_DIR}/distro-images
	rm -rf ${TOP_DIR}/initrd ${TOP_DIR}/rootfs
	popd
}

do_package ()
{
	echo
	echo -e "${GREEN}Packing Buildroot for $PLATFORM on [`date`]${NORMAL}"
	echo

	cp $BUILDROOT_PATH/output/images/rootfs.ext4 $OUTPUT_PLATFORM_DIR/host-fs.ext4
}


DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $DIR/framework.sh $@
