#!/usr/bin/env bash

# Copyright (c) 2015-2023, ARM Limited and Contributors. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# Neither the name of ARM nor the names of its contributors may be used
# to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

#
# This script uses the following environment variables from the variant
#
# VARIANT - build variant name
# TOP_DIR - workspace root directory
# LINUX_COMPILER - PATH to GCC including CROSS-COMPILE prefix
# TFTF_BUILD_ENABLED - Flag to enable building ARM TFTF
# TFTF_RME_BUILD_ENABLED - Flag to enable building ARM Trusted Firmware with
# 	support for RME
# TFTF_PATH - sub-directory containing ARM TFTF code
# TFTF_PLATS - List of platforms to be built (from available in tftf/plat)
# TFTF_DEBUG_ENABLED - 1 = debug, 0 = release build
# TFTF_BUILD_TYPE - The type of build to perform
# TFTF_GCC_PATH - Path where the compiler is installed
# VARIANT_ELF - The compiler variant
# OUTPUT_DIR - Directory where build products are stored
#

do_build ()
{
	echo
	echo -e "${GREEN}Building TFTF for $PLATFORM on [`date`]${NORMAL}"
	echo

	if [ "$TFTF_BUILD_ENABLED" == "1" ]; then
		export CROSS_COMPILE=$TOP_DIR/$LINUX_COMPILER

		pushd $TOP_DIR/$TFTF_PATH
		for plat in $TFTF_PLATS; do
			make -j $PARALLELISM PLAT=${plat} DEBUG=${TFTF_DEBUG_ENABLED} TEST_REPORTS="${TFTF_REPORTS}"
		done
		popd
	fi

	if [ "$TFTF_RME_BUILD_ENABLED" == "1" ]; then
		pushd $TFTF_PATH
		# Set Compiler
		export CROSS_COMPILE=$VARIANT_ELF-
		export PATH=$TFTF_GCC_PATH:$PATH

		# Set debug flag, based on user choice
		local tftf_debug_flag
		[ "${TFTF_BUILD_TYPE}" == "debug" ] && tftf_debug_flag=1 || tftf_debug_flag=0

		# Build TFTF for Standard Tests. Always build this default target,
		# because TF-A needs the generated 'sp_layout.json' for secure 
		# partition.
		make realclean
		make -j $PARALLELISM \
			PLAT=$TFTF_PLATS \
			ARCH=$TFTF_ARCH \
			DEBUG=$tftf_debug_flag \
			USE_NVM=0 \
			SHELL_COLOR=1 \
			ENABLE_REALM_PAYLOAD_TESTS=1 \
			all
		cp ${TFTF_PATH}/build/${TFTF_PLATS}/${TFTF_BUILD_TYPE}/tftf.bin $OUTPUT_PLATFORM_DIR/tftf-std-tests.bin
		popd
	fi
}

do_clean ()
{
	echo
	echo -e "${GREEN}Cleaning TFTF for $PLATFORM on [`date`]${NORMAL}"
	echo

	if [ "$TFTF_BUILD_ENABLED" == "1" ]; then
		export CROSS_COMPILE=$TOP_DIR/$LINUX_COMPILER

		pushd $TOP_DIR/$TFTF_PATH

		for plat in $TFTF_PLATS; do
			make PLAT=$plat DEBUG=$TFTF_DEBUG_ENABLED clean
		done
		popd
	fi

	if [ "$TFTF_RME_BUILD_ENABLED" == "1" ]; then
		pushd $TFTF_PATH
		make realclean
		popd

		# Remove the bins copied to output-products
		rm -rf $OUTPUT_PLATFORM_DIR/tftf-std-tests.bin
	fi
}

do_package ()
{
	echo
	echo -e "${GREEN}Packaging TFTF for $PLATFORM on [`date`]${NORMAL}"
	echo

	if [ "$TFTF_BUILD_ENABLED" == "1" ]; then
		echo "Packaging tftf... $VARIANT";
		# Copy binaries to output folder
		for plat in $TFTF_PLATS; do
			mkdir -p ${OUTDIR}/$plat
			local mode=release
			if [ "$TFTF_DEBUG_ENABLED" == "1" ]; then
				mode=debug
			fi
			cp ${TOP_DIR}/${TFTF_PATH}/build/${plat}/${mode}/tftf.bin ${OUTDIR}/${plat}/
		done
	fi

	if [ "$TFTF_RME_BUILD_ENABLED" == "1" ]; then
		echo
		# Dummy call, tftf is packed into TF-A FIP binary.
	fi
}

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $DIR/framework.sh $@
